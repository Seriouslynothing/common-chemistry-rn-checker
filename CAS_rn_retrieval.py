# -*- coding: utf-8 -*-
"""
Created on Wed Mar 31 19:25:48 2021

@author: Nothingserious
"""

import pandas as pd
import requests
import time

df = pd.read_excel('input.xlsx')
dfdict = df.to_dict()

numberofentries = len(dfdict['Name'])
calctime = numberofentries

print('Request time approx. %i seconds.' % calctime)

for i in dfdict['Name']:
    param = dfdict['Name'][i]
    url = f'https://commonchemistry.cas.org/api/search?q={param}'
    r = requests.get(url)
    if r.json()['results'] == []:
        dfdict['CAS'][i] = 0
    else:
        rson = r.json()['results'][0]['rn']
        dfdict['CAS'][i] = rson
    time.sleep(1)

df = pd.DataFrame.from_dict(dfdict)

df.to_excel('output.xlsx', index=False)
