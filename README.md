# Common Chemistry RN checker

A short python script to check chemical names for CAS registry numbers with the CAS Common Chemistry API.

Uses input.xlsx with a list of chemical names to check against https://commonchemistry.cas.org/api/ and writes output.xlsx with names and CAS numbers.
